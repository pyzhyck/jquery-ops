(function($) {

  $.fn.ops = function(options) {

    return this.each(function(i, el) {
      // default settings
      var settings = $.extend({
        scrollSpeed: 2,
        startScreenIndex: 0,
        ease: 'ease-in-out',
        destroyTo: 992,
        afterChange: function(currentScreen, scrollDirrection) { },
        beforeChange: function(currentScreen, scrollDirrection) { },
        onDestroy: function() { },
        onInit: function() { }
      }, options);

      var scrollDirrection = '';
      var currentIndex = settings.startScreenIndex;
      var screenHeight = $(window).innerHeight();
      var totalScreens = 0;
      var isScrolling = false;
      var isContentLong = false;
      var initedTimes = 0;
      var _this = el;

      if($(window).width() > settings.destroyTo) {
        init();
      } else {
        destroy();
      }

      // Public methods
      $.fn.ops_gotoScreen = function(screenIndex) {
        goToScreen(screenIndex);
      }
      $.fn.ops_nextScreen = function() {
        goToNextScreen();
      }
      $.fn.ops_prevScreen = function() {
        goToPrevScreen();
      }
      $.fn.ops_refresh = function() {
        init();
      }
      $.fn.ops_destroy = function() {
        destroy();
      }

      // Check for document resizing
      $(window).on('resize', function() {
        screenHeight = $(window).innerHeight();
        if($(window).width() > settings.destroyTo) {
          init();
        } else {
          destroy();
        }
      });

      // Private method to scroll to the screen index
      function goToScreen(screenIndex) {
        if(screenIndex >= 0 && screenIndex <= totalScreens) {
          isScrolling = true;
          settings.beforeChange(currentIndex);
          $(_this).children().css({
            'transform': 'translate3d(0, -' + currentIndex * screenHeight + 'px, 0)'
          });
          setTimeout(function() {
            isScrolling = false;
            currentIndex = screenIndex;
            settings.afterChange(currentIndex);
          }, settings.scrollSpeed * 1000);
        } else {
          console.warn('Index ' + screenIndex + ' out of screens count! You have maximum ' + totalScreens + ' screens.');
        }
      }

      // Private method to scroll to the next screen
      function goToNextScreen() {
        isScrolling = true;
        scrollDirrection = 'scroll-down';
        settings.beforeChange(currentIndex, scrollDirrection);
        if(currentIndex < totalScreens) {
          ++currentIndex;
          $(_this).children().css({
            'transform': 'translate3d(0, -' + currentIndex * screenHeight + 'px, 0)'
          });
          setTimeout(function() {
            isScrolling = false;
            settings.afterChange(currentIndex, scrollDirrection);
          }, settings.scrollSpeed * 1000);
        } else {
          isScrolling = false;
        }
      }

      // Private method to scroll to the previous screen
      function goToPrevScreen() {
        isScrolling = true;
        scrollDirrection = 'scroll-up';
        settings.beforeChange(currentIndex, scrollDirrection);
        if(currentIndex > 0) {
          --currentIndex;
          $(_this).children().css({
            'transform': 'translate3d(0, -' + currentIndex * screenHeight + 'px, 0)'
          });
          setTimeout(function() {
            isScrolling = false;
            settings.afterChange(currentIndex, scrollDirrection);
          }, settings.scrollSpeed * 1000);
        } else {
          isScrolling = false;
        }
      }

      // Private method to destroy
      function destroy() {
        totalScreens = 0;
        isScrolling = false;
        isContentLong = false;
        $(_this).off();
        $('*[data-ops-long-content]').off();
        $('*[data-ops="ops-child"]').removeAttr('style');
        $(_this).addClass('destroy');
        settings.onDestroy();
      }

      // Private method to init plugin
      function init() {
        $(_this).removeClass('destroy');
        $(_this).attr('data-ops', 'ops-wraper');
        $(_this).children().attr('data-ops', 'ops-child');
        if(initedTimes == 0) {
          goToScreen(settings.startScreenIndex);
        }
        setTimeout(function() {
          $(_this).find('*[data-ops="ops-child"]').each(function(index, item) {
            totalScreens = index;
            $(item).attr('data-ops-child-index', index);
            $(item).css({
              'transition': 'all ' + settings.scrollSpeed + 's ' + settings.ease
            });
            var currentChildContentHeight = 0;
            $(item).children().each(function(i, el) {
              currentChildContentHeight += $(el).outerHeight();
            });
            if(currentChildContentHeight > screenHeight) $(item).attr('data-ops-long-content', true);
          });
          $(_this).on('DOMMouseScroll mousewheel wheel', function(e) {
            if($(_this).attr('data-ops') == 'ops-wraper' && !isScrolling && !isContentLong) {
              if(e.originalEvent.deltaY > 0) {
                goToNextScreen();
              } else {
                goToPrevScreen();
              }
              if($('*[data-ops-child-index="' + currentIndex + '"]').attr('data-ops-long-content')) {
                isContentLong = true;
              } else {
                isContentLong = false;
              }
            }
          });
          $('*[data-ops-long-content]').on('DOMMouseScroll mousewheel wheel', function(e) {
            if(!isScrolling) {
              var bottomScrollPos = $(this)[0].scrollHeight - screenHeight;
              var currentScrollPos = $(this).scrollTop();
              if(currentScrollPos == bottomScrollPos && e.originalEvent.deltaY > 0) {
                goToNextScreen();
              } else if(currentScrollPos == 0 && e.originalEvent.deltaY < 0) {
                goToPrevScreen();
              }
              if($('*[data-ops-child-index="' + currentIndex + '"]').attr('data-ops-long-content')) {
                isContentLong = true;
              } else {
                isContentLong = false;
              }
            }
          });
          ++initedTimes;
          $(_this).addClass('init');
          settings.onInit();
        }, settings.scrollSpeed * 1000)
      }
    });
  }

})(jQuery);