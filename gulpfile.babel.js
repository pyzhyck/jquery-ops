import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import rename from 'gulp-rename';
import cleanCss from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';
import browserSync from 'browser-sync';
import wait from 'gulp-wait';

let _browserSync = browserSync.create();
let serverConf = {
  server: {
    baseDir: "./",
    serveStaticOptions: {
      extensions: ["html"]
    }
  },
  startPath: "./example",
  notify: false,
  open: true,
  ghostMode: false,
  host: "10.0.1.21"
}

function serverStart() {
  _browserSync.init(serverConf);
}

function serverReload(cb) {
  _browserSync.reload();
  cb();
}


export function sassCompiler() {
  return gulp.src('./src/jquery-ops.scss')
    .pipe(wait(300))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browserlist: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./dist'));
    
}

export function minCss() {
  return gulp.src('./dist/jquery-ops.css')
    .pipe(sourcemaps.init())
      .pipe(cleanCss())
    .pipe(sourcemaps.write())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./dist'));
    
}

export function moveJS() {
  return gulp.src('./src/jquery-ops.js')
    .pipe(gulp.dest('./dist'));
    
}

export function minJS() {
  return gulp.src('./dist/jquery-ops.js')
    .pipe(sourcemaps.init())
      .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./dist'));
    
}

export function watcher() {
  gulp.watch(['./src/jquery-ops.scss'], gulp.series(sassCompiler, minCss, serverReload));
  gulp.watch(['./src/jquery-ops.js'], gulp.series(moveJS, minJS, serverReload));
  gulp.watch(['./example/index.html'], gulp.series(serverReload));
}

gulp.task('dev', 
  gulp.series(
    sassCompiler,
    minCss,
    moveJS,
    minJS,
    gulp.parallel(watcher, serverStart)
  )
);

gulp.task('build', 
  gulp.series(
    sassCompiler, 
    minCss, 
    moveJS, 
    minJS
  )
);