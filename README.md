## Requirement

jQuery 3.x

## Browser support

- Modern browsers: Chrome, Firefox, and Safari. 
- Also work fine on IE11 and Edge.

## Basic Usage

- Add jquery-ops.min.css from /dist directory into your document's `<head>` section.
- Add jquery-ops.min.js from /dist directory to the bottom of your document's `<body>` section.
- Create element that will be container for your "screens"

````html
<head>
  ...
  <link rel="stylesheet" href="path/to/jquery-ops.min.css">
  ...
</head>
<body>
  ...
  <div class="jquery-ops-container">
    <section>...</section>
    <section>...</section>
    ...
    or you can put any type of html tag, such as div
    <div>...</div>
    ...
  </div>
  ...
  <script src="path/to/jquery-ops.min.js"></script>
</body>
````
- Call the function to activate plugin:

````javascript
$(".jquery-ops-container").ops({
  easing: "ease-in-out",                       // Transition timing function
  scrollSpeed: 2,                              // Scroll speed between screens in secconds
  startScreenIndex: 0,                         // Start from screen(0 - is the first screen)
  destroyTo: 992,                              // Plugin not working when screen width less then this value
  beforeChange: function(screenIndex, scrollDirrection) { ... }, // Fired before scrolling to another screen
  afterChange: function(screenIndex, scrollDirrection) { ... },  // Fired after scrolling to another screen
  onDestroy: function(screenIndex) { ... },    // Fired when plugin stop working
  onInit: function(screenIndex) { ... }        // Fired when plugin start working
});
````

## Methods

````javascript
$(".jquery-ops-container").ops_gotoScreen(screenIndex); // Scrolling to screen with index...
$(".jquery-ops-container").ops_nextScreen();            // Scrolling to the next screen
$(".jquery-ops-container").ops_prevScreen();            // Scrolling to the previous screen
$(".jquery-ops-container").ops_refresh();               // Reinit plugin
$(".jquery-ops-container").ops_destroy();               // Destroy plugin
````